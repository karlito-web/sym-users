<?php

namespace KarlitoWeb\Users\Service;

use KarlitoWeb\Users\Entity\User;
use KarlitoWeb\Users\Repository\UserRepository;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

final readonly class OAuthRegistrationService
{
    /**
     * @param ResourceOwnerInterface $resourceOwner
     * @param UserRepository $repository
     * @return User
     */
    public function persist(ResourceOwnerInterface $resourceOwner, UserRepository $repository): User
    {
        $user = match (true) {
            $resourceOwner instanceof GithubResourceOwner => (new User())
                ->setEmail($resourceOwner->getEmail())
                ->setGithubId($resourceOwner->getId()),
            $resourceOwner instanceof GoogleUser => (new User())
                ->setEmail($resourceOwner->getEmail())
                ->setGoogleId($resourceOwner->getId()),
        };

        $repository->add($user, true);

        return $user;
    }
}
