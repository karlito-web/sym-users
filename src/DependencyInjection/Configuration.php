<?php

namespace KarlitoWeb\Users\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package KarlitoWeb\Users\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder(): TreeBuilder
    {
		$treeBuilder = new TreeBuilder('Users');
		$rootNode = $treeBuilder->getRootNode();

		 $rootNode
            ->children()
		 	    ->scalarNode('foo')
		 	    ->defaultValue('Users Bundle')
		 	    ->info('azerty')
		 	    ->isRequired()
		 	->end();

		return $treeBuilder;
	}
}
