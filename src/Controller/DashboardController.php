<?php

namespace KarlitoWeb\Users\Controller;

use KarlitoWeb\Users\DTO\UserSearch;
use KarlitoWeb\Users\Form\SearchUserType;
use KarlitoWeb\Users\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users', name: 'kw.users.dashboard.', methods: ['GET'], format: 'html', utf8: true)]
final class DashboardController extends AbstractController
{
	#[Route(path: '/dashboard.php', name: 'index', options: ['expose' => true])]
	public function index(Request $request): Response
	{
        $r = new UserSearch();
        $form = $this->createForm(SearchUserType::class, $r)->handleRequest($request);

		return $this->render('@Users/contents/dashboard/index.html.twig', [
			'controller_name' => 'Welcome to Dashboard Users',
			'form'            => $form->createView(),
		]);
	}

    /**
     * Interroge la BDD pour récupérer un utilisateur
     */
    #[Route('/search.php', name: 'search', methods: ['POST'])]
    public function search(Request $request, UserRepository $repository): Response
    {
        /**
         * Récupère la valeur de la requête
         *
         * @var string $search
         */
        $search = $request->request->get('search');

        if (strlen($search) < 2) {
            return $this->render('@Users/contents/dashboard/_search.html.twig', [
                'search' => $repository->findBy([]),
            ]);
        } else {
            return $this->render('@Users/contents/dashboard/_search.html.twig', [
                'search' => $repository->findUserBySearch($search),
            ]);
        }
    }
}
