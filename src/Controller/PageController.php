<?php

namespace KarlitoWeb\Users\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users', name: 'kw.users.page.', methods: ['GET'], format: 'html', utf8: true)]
final class PageController extends AbstractController
{
    #[Route(path: '/terms.html', name: 'terms')]
    public function terms(): Response
    {
        $this->render('@Users/page/terms.html.twig', [
            'controller_name' => 'Terms',
        ]);
    }

    #[Route(path: '/privacy.html', name: 'privacy')]
    public function privacy(): Response
    {
        $this->render('@Users/page/privacy.html.twig', [
            'controller_name' => 'Privacy',
        ]);
    }
}
