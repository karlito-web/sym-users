<?php

namespace KarlitoWeb\Users\Controller\SocialConnect;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class GithubController
 *
 * @package KarlitoWeb\Users\Controller\SocialConnect
 */
#[Route(path: '/users/connect', name: 'kw.users.connect.', format: 'html', utf8: true)]
final class GithubController extends AbstractController
{
    public const SCOPES = [
        'github_main' => [
            'user:email'
        ],
    ];

    #[Route(path: '/{service}.php', name: 'github', methods: ['GET'])]
    public function connect(string $service, ClientRegistry $clientRegistry): RedirectResponse
    {
        if (!in_array($service, array_keys(self::SCOPES), true)) {
            throw $this->createNotFoundException();
        }

        return $clientRegistry
            ->getClient($service)
            ->redirect(self::SCOPES[$service]);
    }

    #[Route('/oauth/check/{service}', name: 'oauth.check', methods: ['GET', 'POST'])]
    public function check(): Response
    {
        return new Response(status: 200);
    }
}
