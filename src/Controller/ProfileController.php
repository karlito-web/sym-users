<?php

namespace KarlitoWeb\Users\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use KarlitoWeb\Users\Entity\User;
use KarlitoWeb\Users\Form\UserCreateType;
use KarlitoWeb\Users\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ProfileController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users/profile', name: 'kw.users.profile.', methods: ['GET'], format: 'html', utf8: true)]
final class ProfileController extends AbstractController
{
    public function __construct(
        private readonly AuthorizationCheckerInterface $authChecker,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
    ) {
    }

    #[Route(path: '/create.php', name: 'create', requirements: ['uuid' => Requirement::ASCII_SLUG], options: ['expose' => true], methods: ['GET', 'POST'])]
	public function create(Request $request, UserRepository $repository): Response
	{
        /** Check if User is an Admin */
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) :
            $this->addFlash('warning', 'Access denied ! You need to be an administrator.');

            return $this->redirectToRoute('kw.users.login', [], Response::HTTP_SEE_OTHER);
        endif;

        /** Create User */
        $user      = new User();

        /** Create Form */
        $form = $this->createForm(UserCreateType::class, $user)->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var User $data */
                $data = $form->getData();
                $this->addFlash('success', $data->getFirstname() . ' ' . $data->getLastname() . ' is saved successfully!');
                $repository->add($user, true);
            } else {
                $this->addFlash('danger', 'Form is invalid !');
            }
        }

        return $this->render('@Users/contents/profile/create.html.twig', [
			'controller_name'   => 'Create a Profile',
			'form'              => $form->createView(),
		]);
	}

    /**
     * @throws Exception
     */
    #[Route(path: '/create-fake.php', name: 'create.fake', requirements: ['uuid' => Requirement::ASCII_SLUG], options: ['expose' => true], methods: ['GET', 'POST'])]
	public function createFake(UserRepository $repository, HttpClientInterface $client): Response
	{
        /** Check if User is an Admin */
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) :
            $this->addFlash('warning', 'Access denied ! You need to be an administrator.');

            return $this->redirectToRoute('kw.users.login', [], Response::HTTP_SEE_OTHER);
        endif;

        /** Request to the api */
        try {
            $response = $client->request('GET', 'https://randomuser.me/api/?nat=fr');
        } catch (TransportExceptionInterface $e) {
            throw new Exception($e->getMessage());
        }
        try {
            $datas = json_decode($response->getContent(), true)['results'][0];
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            throw new Exception($e->getMessage());
        }

        /** Create User */
        $user = new User();
        $user->setUsername($datas['login']['username']);
        $user->setFirstname($datas['name']['first']);
        $user->setLastname($datas['name']['last']);
        $user->setemail($datas['email']);
        $user->setuuid($datas['login']['uuid']);
        $user->setPlainPassword('password');
		$user->setPassword(
			$this->userPasswordHasher->hashPassword($user, 'password')
		);
        $user->setRoles(['ROLE_USER']);

        /** Save User & Flash message */
        $repository->add($user, true);
        $this->addFlash('success', $datas['name']['first'] . ' ' . $datas['name']['last'] . ' is saved successfully!');

        return $this->redirectToRoute('kw.users.dashboard.index');
	}

	#[Route(path: '/update.php/{uuid}', name: 'update', requirements: ['uuid' => Requirement::ASCII_SLUG], options: ['expose' => true], methods: ['GET', 'POST'])]
	public function update(Request $request, UserRepository $repository): Response
	{
        /** Check if User is an Admin */
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) :
            $this->addFlash('warning', 'Access denied ! You need to be an administrator.');

            return $this->redirectToRoute('kw.users.login', [], Response::HTTP_SEE_OTHER);
        endif;

		return $this->render('@Users/contents/profile/update.html.twig', [
			'controller_name'   => 'Edit Profile',
			'user'              => $repository->findOneBy(['uuid' => $request->attributes->get('uuid')]),
		]);
	}

    #[Route('/delete.php/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        /** Check if User is an Admin */
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) :
            $this->addFlash('warning', 'Access denied ! You need to be an administrator.');

            return $this->redirectToRoute('kw.users.login', [], Response::HTTP_SEE_OTHER);
        endif;

        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->getPayload()->getString('_token'))) {
            $this->addFlash('success', 'User has been deleted successfully !');
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('kw.users.dashboard.index', [], Response::HTTP_SEE_OTHER);
    }
}
