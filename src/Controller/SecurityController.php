<?php

namespace KarlitoWeb\Users\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users', name: 'kw.users.', methods: ['GET', 'POST'], format: 'html', utf8: true)]
final class SecurityController extends AbstractController
{
    #[Route(path: '/login.php', name: 'login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        /** Already Log */
		if ($this->getUser()) {
		    $this->addFlash('info', 'You are already logged !');
            return $this->redirectToRoute('kw.users.dashboard.index');
		}

        return $this->render('@Users/contents/security/login.html.twig', [
            'controller_name'   => 'Sign-In',
            // last username entered by the user
            'last_username'     => $authenticationUtils->getLastUsername(),
            // get the login error if there is one
            'error'             => $authenticationUtils->getLastAuthenticationError()
        ]);
    }

    #[Route(path: '/logout.php', name: 'logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
