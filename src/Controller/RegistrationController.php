<?php

namespace KarlitoWeb\Users\Controller;

use Doctrine\ORM\EntityManagerInterface;
use KarlitoWeb\Users\Entity\User;
use KarlitoWeb\Users\Form\RegistrationFormType;
use KarlitoWeb\Users\Repository\UserRepository;
use KarlitoWeb\Users\Security\EmailVerifier;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RegistrationController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users', name: 'kw.users.registration.', format: 'html', utf8: true)]
final class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly EmailVerifier $emailVerifier,
        private readonly TranslatorInterface $translator,
    ) {}

    #[Route(path: '/register.php', name: 'register')]
    public function register(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $entityManager
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var string $plainPassword */
            $plainPassword = $form->get('plainPassword')->getData();

            // encode the plain password
            $user->setPassword($userPasswordHasher->hashPassword($user, $plainPassword));

            // add default Role
            $user->setRoles(['ROLE_USER']);

            // save in database
            $entityManager->persist($user);
            $entityManager->flush();

			// generate a flash bag message
			$this->addFlash('success', $this->translator->trans('flash.account.created', [], 'flashes', $this->translator->getLocale()));

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('kw.users.registration.verify.email', $user,
                (new TemplatedEmail())
                    ->from(new Address('webmaster@karlito-web.com', 'Dark Vador'))
                    ->to((string) $user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('@Users/emails/confirmation-email.html.twig')
            );

            // do anything else you need here, like send an email
            /**
             * You authenticate directly user after registration
             *
			return $userAuthenticator->authenticateUser(
				$user,
				$authenticator,
				$request
			);
             */

            /**
             * You need to re authenticate user
             */
            return $this->redirectToRoute('kw.users.login');
        }

        return $this->render('@Users/contents/security/register.html.twig', [
			'controller_name'  => 'Sign-In',
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/verify-account/send-email.php', name: 'verify.email')]
    public function verifyUserEmail(Request $request, TranslatorInterface $translator, UserRepository $userRepository): Response
    {
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $id = $request->query->get('id');

        if (null === $id) {
            return $this->redirectToRoute('kw.users.registration.register');
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('kw.users.registration.register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('kw.users.registration.register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', $this->translator->trans('flash.account.verified', [], 'flashes', $this->translator->getLocale()));

        return $this->redirectToRoute('kw.users.dashboard.index');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/verify-account/resend-email.php', name: 'resend.email')]
	public function reSendEmail(): RedirectResponse
    {
		/** @var User $user */
		$user = $this->getUser();

		if ($user->isVerified() !== true) {
			// generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('kw.users.registration.verify.email', $user,
				(new TemplatedEmail())
                    ->from(new Address('webmaster@karlito-web.com', 'Dark Vador'))
                    ->to((string) $user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('@Users/emails/confirmation-email.html.twig')
			);
			// do anything else you need here, like send an email

			// generate a flash bag message
			$this->addFlash('info', $this->translator->trans('flash.account.resend', [], 'flashes', $this->translator->getLocale()));

		}

		return $this->redirectToRoute('kw.users.login');
	}
}
