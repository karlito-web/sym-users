<?php

namespace KarlitoWeb\Users\Controller;

use KarlitoWeb\Users\Entity\User;
use KarlitoWeb\Users\Form\ChangePasswordFormType;
use KarlitoWeb\Users\Form\ResetPasswordRequestFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * Class ResetPasswordController
 *
 * @package KarlitoWeb\Users\Controller
 */
#[Route(path: '/users', name: 'kw.users.', format: 'html', utf8: true)]
class ResetPasswordController extends AbstractController
{
	use ResetPasswordControllerTrait;

	public function __construct(
		private readonly EntityManagerInterface $entityManager,
        private readonly MailerInterface $mailer,
		private readonly ResetPasswordHelperInterface $resetPasswordHelper,
        private readonly TranslatorInterface $translator,
	) {}

	/**
	 * Confirmation page after a user has requested a password reset.
	 */
	#[Route('/check-email.php', name: 'check.email')]
	public function checkEmail(): Response
	{
		// Generate a fake token if the user does not exist or someone hit this page directly.
		// This prevents exposing whether or not a user was found with the given email address or not
		if (null === ($resetToken = $this->getTokenObjectFromSession())) {
			$resetToken = $this->resetPasswordHelper->generateFakeResetToken();
		}

		return $this->render('@Users/contents/reset-password/check-email.html.twig', [
			'controller_name' => 'Password Reset Email Sent',
			'resetToken'      => $resetToken,
		]);
	}

    /**
     * Display & process form to request a password reset.
     *
     * @throws TransportExceptionInterface
     */
	#[Route('/reset-password.php', name: 'forgot.password')]
	public function request(Request $request): Response
	{
		$form = $this->createForm(ResetPasswordRequestFormType::class)->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			return $this->processSendingPasswordResetEmail($form->get('email')->getData(), $this->mailer);
		}

		return $this->render('@Users/contents/reset-password/request.html.twig', [
			'controller_name' => 'Reset your password',
			'requestForm'     => $form->createView(),
		]);
	}

	/**
	 * Validates and process the reset URL that the user clicked in their email.
	 */
	#[Route('/reset/{token}', name: 'reset.password')]
	public function reset(Request $request, UserPasswordHasherInterface $passwordHasher, string $token = null): Response
	{
		if ($token) {
			// We store the token in session and remove it from the URL, to avoid the URL being
			// loaded in a browser and potentially leaking the token to 3rd party JavaScript.
			$this->storeTokenInSession($token);

			return $this->redirectToRoute('kw.users.reset.password');
		}

		$token = $this->getTokenFromSession();
		if (null === $token) {
			throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
		}

		try {
			/** @var User $user */
			$user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
		} catch (ResetPasswordExceptionInterface $e) {
			$this->addFlash('reset_password_error', sprintf(
				'%s - %s',
				$this->translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE, [], 'ResetPasswordBundle'),
				$this->translator->trans($e->getReason(), [], 'ResetPasswordBundle')
			));

			return $this->redirectToRoute('kw.users.forgot.password');
		}

		// The token is valid; allow the user to change their password.
		$form = $this->createForm(ChangePasswordFormType::class);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			// A password reset token should be used only once, remove it.
			$this->resetPasswordHelper->removeResetRequest($token);

			// Encode(hash) the plain password, and set it.
			$encodedPassword = $passwordHasher->hashPassword($user, $form->get('plainPassword')->getData());

			$user->setPassword($encodedPassword);
			$this->entityManager->flush();

			// The session is cleaned up after the password has been changed.
			$this->cleanSessionAfterReset();

			return $this->redirectToRoute('kw.users.login');
		}

		return $this->render('@Users/contents/reset-password/reset.html.twig', [
			'controller_name' => 'Reset your password',
			'resetForm'       => $form->createView(),
		]);
	}

    /**
     * @param string $emailFormData
     * @param MailerInterface $mailer
     * @return RedirectResponse
     * @throws TransportExceptionInterface
     */
	private function processSendingPasswordResetEmail(string $emailFormData, MailerInterface $mailer): RedirectResponse
	{
        /**
         * Search User
         * @var User $user
         */
		$user = $this->entityManager->getRepository(User::class)->findOneBy([
			'email' => $emailFormData,
		]);

		// Do not reveal whether a user account was found or not.
		if (!$user) {
			return $this->redirectToRoute('kw.users.check.email');
		}

		try {
			$resetToken = $this->resetPasswordHelper->generateResetToken($user);
		} catch (ResetPasswordExceptionInterface $e) {
			// If you want to tell the user why a reset email was not sent, uncomment
			// the lines below and change the redirect to 'kw.users.forgot.password'.
			// Caution: This may reveal if a user is registered or not.
			//
			 $this->addFlash('reset_password_error', sprintf(
			     '%s - %s',
			     $this->translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_HANDLE, [], 'ResetPasswordBundle'),
			     $this->translator->trans($e->getReason(), [], 'ResetPasswordBundle')
			 ));

			return $this->redirectToRoute('kw.users.check.email');
		}

		$email = (new TemplatedEmail())
			->from(new Address('webmaster@karlito-web.com', 'Password Service'))
			->to($user->getEmail())
			->subject('You want to reset your password')
			->htmlTemplate('@Users/emails/reset-password.html.twig')
			->context([
				'resetToken' => $resetToken,
			])
		;

		$mailer->send($email);

		// Store the token object in session for retrieval in check-email route.
		$this->setTokenObjectInSession($resetToken);

		return $this->redirectToRoute('kw.users.check.email');
	}
}
