<?php

namespace KarlitoWeb\Users\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResetPasswordRequestFormType
 *
 * @package KarlitoWeb\Users\Form
 */
class ResetPasswordRequestFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('email', EmailType::class, [
				'attr' => [
					'autocomplete' => 'email',
                    'placeholder' => 'user.form.email.placeholder',
				],
                'label' => 'user.form.email.label',
				'constraints' => [
					new NotBlank([
						'message' => 'Please enter your email',
					]),
				],
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class'            => null,
			'translation_domain'    => 'forms',
			// enable/disable CSRF protection for this form
//			'csrf_protection'       => true,
			// the name of the hidden HTML field that stores the token
//			'csrf_field_name'       => '_token',
			// an arbitrary string used to generate the value of the token using a different string for each form improves its security
//			'csrf_token_id'         => 'security_token',
		]);
	}

    public function getBlockPrefix(): string
    {
        return '';
    }
}
