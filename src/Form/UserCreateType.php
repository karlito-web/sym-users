<?php

namespace KarlitoWeb\Users\Form;

use KarlitoWeb\Users\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class UserCreateType
 *
 * @package KarlitoWeb\Users\Form
 */
class UserCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
                ],
				'constraints' => [
					new Length(min: 0, max: 64),
				],
                'label'    => 'user.form.username.label',
                'required' => false,
                'trim'     => true,
            ])
            ->add('email', EmailType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 255,
					'min' => 8,
				],
				'label'       => 'user.form.email.label',
				'required'    => true,
                'trim'        => true,
				'constraints' => [
					new Email(),
					new Length(min: 8, max: 255),
                    new NotBlank(),
                    new NotNull(),
				],
            ])
            ->add('firstname', TextType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
				],
				'label'       => 'user.form.firstname.label',
				'required'    => false,
                'trim'        => true,
				'constraints' => [
					new Length(min: 0, max: 64),
				],
            ])
            ->add('lastname', TextType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
				],
				'label'       => 'user.form.lastname.label',
				'required'    => false,
                'trim'        => true,
				'constraints' => [
					new Length(min: 0, max: 64),
				],
            ])
//            ->add('roles')
			->add('plainPassword', RepeatedType::class, [
				'type'            => PasswordType::class,
				'invalid_message' => 'The password fields must match.',
                'mapped'          => false,
                'required'        => true,
				'options'         => [
					'attr' => [
						'autocomplete'  => 'new-password',
						'class'         => 'password-field',
						'value'         => 'password',
					]
				],
				'first_options'   => [
					'constraints' => [
						new NotBlank(['message' => 'Please enter a password']),
						new Length([
							'min' => 6,
							'minMessage' => 'Your password should be at least {{ limit }} characters',
							// max length allowed by Symfony for security reasons
							'max' => 4096,
						]),
					],
					'label' => 'user.form.password.label',
				],
				'second_options'  => [
					'label' => 'user.form.password.confirmation.label'
				],
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'            => User::class,
            'translation_domain'    => 'forms',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
