<?php

namespace KarlitoWeb\Users\Form;

use KarlitoWeb\Users\DTO\UserSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class SearchUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search', EmailType::class, [
                'attr'          => [
                    'autocomplete'  => 'off',
                    'block_name'    => 'search',
                    'hx-post'       => '/google/books/search/api.php',
                    'hx-trigger'    => 'keyup changed delay: 750ms, search',
                    'hx-target'     => '#search-results',
                    'id'            => 'searchRequest',
                    'max'           => 255,
                    'min'           => 3,
                    'placeholder'   => 'Saisissez une recherche',
                ],
                'label'         => 'user.form.search.label',
                'required'      => false,
                'trim'          => true,
                'constraints'   => [
                    new Length(min: 3, max: 255),
                    new NotBlank(),
                    new NotNull(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'            => UserSearch::class,
            'translation_domain'    => 'forms',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
