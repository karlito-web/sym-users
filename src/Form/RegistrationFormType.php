<?php

namespace KarlitoWeb\Users\Form;

use KarlitoWeb\Users\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RegistrationFormType
 *
 * @package KarlitoWeb\Users\Form
 */
class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
			->add('username', TextType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
				],
				'data' => 'Akh',
				'label' => 'user.form.username.label',
				'required' => false,
				'constraints' => [
					new Length(min: 0, max: 64),
				]
			])
            ->add('email', EmailType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 255,
					'min' => 8,
				],
				'data' => 'akhenation@iam.fr',
				'label' => 'user.form.email.label',
				'required' => true,
				'constraints' => [
					new Email(),
					new Length(min: 8, max: 255),
				]
			])
			->add('firstname', TextType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
				],
				'data' => 'Philippe',
				'label' => 'user.form.firstname.label',
				'required' => false,
				'constraints' => [
					new Length(min: 0, max: 64),
				]
			])
			->add('lastname', TextType::class, [
				'attr' => [
					'autocomplete' => 'off',
					'max' => 64,
					'min' => 0,
				],
				'data' => 'FRAGGIONE',
				'label' => 'user.form.lastname.label',
				'required' => false,
				'constraints' => [
					new Length(min: 0, max: 64),
				]
			])
            ->add('agreeTerms', CheckboxType::class, [
				'label'         => 'user.form.agree.label',
                'mapped'        => false,
                'constraints'   => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
			->add('plainPassword', RepeatedType::class, [
				'type'            => PasswordType::class,
				'invalid_message' => 'The password fields must match.',
                'mapped'          => false,
                'required'        => true,
				'options'         => [
					'attr' => [
						'autocomplete'  => 'new-password',
						'class'         => 'password-field',
						'value'         => 'password',
					]
				],
				'first_options'   => [
					'constraints' => [
						new NotBlank(['message' => 'Please enter a password']),
						new Length([
							'min' => 6,
							'minMessage' => 'Your password should be at least {{ limit }} characters',
							// max length allowed by Symfony for security reasons
							'max' => 4096,
						]),
					],
					'label' => 'user.form.password.label',
				],
				'second_options'  => [
					'label' => 'user.form.password.confirmation.label'
				],
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'            => User::class,
			'translation_domain'    => 'forms',
			// enable/disable CSRF protection for this form
//			'csrf_protection'       => true,
			// the name of the hidden HTML field that stores the token
//			'csrf_field_name'       => '_token',
			// an arbitrary string used to generate the value of the token using a different string for each form improves its security
//			'csrf_token_id'         => 'security_token',
        ]);
    }
}
