<?php

namespace KarlitoWeb\Users\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ChangePasswordFormType
 *
 * @package KarlitoWeb\Users\Form
 */
class ChangePasswordFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('plainPassword', RepeatedType::class, [
				'type'              => PasswordType::class,
				'options'           => [
					'attr' => [
						'autocomplete' => 'new-password',
					],
				],
				'first_options'     => [
					'constraints' => [
						new NotBlank([
							'message' => 'Please enter a password',
						]),
						new Length([
							'min' => 6,
							'minMessage' => 'Your password should be at least {{ limit }} characters',
							// max length allowed by Symfony for security reasons
							'max' => 4096,
						]),
					],
					'label' => 'New password',
				],
				'second_options'    => [
					'label' => 'Repeat Password',
				],
				'invalid_message'   => 'The password fields must match.',
				// Instead of being set onto the object directly, this is read and encoded in the controller
				'mapped'            => false,
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class'            => null,
			'translation_domain'    => 'forms',
			// enable/disable CSRF protection for this form
//			'csrf_protection'       => true,
			// the name of the hidden HTML field that stores the token
//			'csrf_field_name'       => '_token',
			// an arbitrary string used to generate the value of the token using a different string for each form improves its security
//			'csrf_token_id'         => 'security_token',
		]);
	}
}
