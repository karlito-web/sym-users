<?php

namespace KarlitoWeb\Users;

use KarlitoWeb\Users\DependencyInjection\UsersExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class UsersBundle
 *
 * @package KarlitoWeb\Users\
 */
class UsersBundle extends Bundle
{
    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return parent::getNamespace();
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    /**
     * @return ExtensionInterface|null
     */
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new UsersExtension();
    }
}
