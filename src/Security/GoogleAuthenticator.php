<?php

namespace KarlitoWeb\Users\Security;

use KarlitoWeb\Users\Entity\User;
use KarlitoWeb\Users\Repository\UserRepository;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class GoogleAuthenticator extends AbstractoAuthAuthenticator
{
    protected string $serviceName = 'google_main';

    /**
     * @param ResourceOwnerInterface $resourceOwner
     * @param UserRepository $repository
     * @return User|null
     */
    protected function getUserFromResourceOwner(ResourceOwnerInterface $resourceOwner, UserRepository $repository): ?User
    {
        if (!($resourceOwner instanceof GoogleUser)) {
            throw new \RuntimeException("expecting google user");
        }

        if (true !== ($resourceOwner->toArray()['email_verified'] ?? null)) {
            throw new AuthenticationException("email not verified");
        }

        return $repository->findOneBy([
            'googleId'  => $resourceOwner->getId(),
            'email'     => $resourceOwner->getEmail()
        ]);
    }
}
