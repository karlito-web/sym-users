<?php

namespace KarlitoWeb\Users\Repository;

use KarlitoWeb\Users\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, User::class);
	}

	/**
	 * Used to upgrade (rehash) the user's password automatically over time.
	 */
	public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
	{
		if (!$user instanceof User) {
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
		}

		$user->setPassword($newHashedPassword);
		$this->getEntityManager()->persist($user);
		$this->getEntityManager()->flush();
	}

    /**
     * To Log user with email or username
     *
     * @param string $query
     * @return User|null
     */
    public function findUserByEmailOrUsername(string $query): ?User
    {
        return $this
            ->createQueryBuilder('u')
            ->where('u.email = :identifier')
            ->orWhere('u.username = :identifier')
            ->setParameter('identifier', $query)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * To Search users with email, username, firstname or lastname
     *
     * @param string $query
     * @return User<>|null
     */
    public function findUserBySearch(string $query): ?array
    {
        return $this
            ->createQueryBuilder('u')
            ->where('u.email LIKE :identifier')
            ->orWhere('u.username LIKE :identifier')
            ->orWhere('u.firstname LIKE :identifier')
            ->orWhere('u.lastname LIKE :identifier')
            ->setParameter('identifier', "%$query%")
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Add a new User
     *
     * @param User $user
     * @param bool $flush
     * @return void
     */
    public function add(User $user, bool $flush = false): void
    {
        $this->getEntityManager()->persist($user);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
