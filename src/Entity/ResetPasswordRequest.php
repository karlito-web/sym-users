<?php

namespace KarlitoWeb\Users\Entity;

use KarlitoWeb\Users\Repository\ResetPasswordRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestTrait;

/**
 * Class ResetPasswordRequest
 *
 * @package KarlitoWeb\Users\Entity
 */
#[ORM\Entity(repositoryClass: ResetPasswordRequestRepository::class)]
#[ORM\Table(name: 'account_reset_password_request')]
class ResetPasswordRequest implements ResetPasswordRequestInterface
{
	use ResetPasswordRequestTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column]
	private ?int $id = null;

	#[ORM\ManyToOne]
	#[ORM\JoinColumn(nullable: false)]
	private ?User $user = null;

	public function __construct(User $user, \DateTimeInterface $expiresAt, string $selector, string $hashedToken)
	{
		$this->user = $user;
		$this->initialize($expiresAt, $selector, $hashedToken);
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUser(): User
	{
		return $this->user;
	}
}
