<?php

namespace KarlitoWeb\Users\Entity;

use KarlitoWeb\Users\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * Class User
 *
 * @package KarlitoWeb\Users\Entity
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'account_user')]
#[UniqueEntity(fields: 'email', message: 'There is alrealdy an account with this {{ label }} in DataBase')]
#[UniqueEntity(fields: 'username', message: 'There is alrealdy an account with this {{ label }} in DataBase')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer', options: ['unsigned' => true])]
	private ?int $id = null;

	#[ORM\Column(type: 'string', length: 180, unique: true)]
	#[Assert\Uuid(strict: true)]
	private ?string $uuid = null;

	#[ORM\Column(type: 'string', length: 64, nullable: true, options: ['default' => null])]
	#[Assert\Length(min: 0, max: 64)]
	#[Assert\Type('string')]
	private ?string $username = null;

	#[ORM\Column(type: 'string', length: 255, nullable: false)]
	#[Assert\Email]
	#[Assert\Length(min: 8, max: 255)]
	#[Assert\Type('string')]
	private string $email;

	#[ORM\Column(type: 'string', length: 64, nullable: true, options: ['default' => null])]
	#[Assert\Length(min: 0, max: 64)]
	#[Assert\Type('string')]
	private ?string $firstname = null;

	#[ORM\Column(type: 'string', length: 60, nullable: true, options: ['default' => null])]
	#[Assert\Length(min: 0, max: 64)]
	#[Assert\Type('string')]
	private ?string $lastname = null;

	#[ORM\Column(type: 'json')]
	#[Assert\NotNull]
	private array $roles = [];

	/**
	 * @var ?string The hashed password
	 */
	#[ORM\Column(type: 'string', length: 255, nullable: true, options: ['default' => null])]
	#[Assert\Length(min: 8, max: 255)]
	#[Assert\Type('string')]
	private ?string $password = null;

	private ?string $plainPassword = null;

	#[ORM\Column(type: 'boolean', options: ['default' => false])]
	private bool $isVerified = false;

	#[ORM\Column(type: 'string', length: 255, nullable: true, options: ['default' => null])]
	private ?string $githubId = null;

	#[ORM\Column(type: 'string', length: 255, nullable: true, options: ['default' => null])]
	private ?string $googleId = null;

	#[ORM\Column(type: 'date_immutable')]
	#[Assert\NotNull]
	private DateTimeImmutable $createdAt;

	#[ORM\Column(type: 'date_immutable')]
	#[Assert\NotNull]
	private DateTimeImmutable $updatedAt;

	#[ORM\Column(type: 'date_immutable', nullable: true, options: ['default' => null])]
	private ?DateTimeImmutable $deletedAt = null;

	public function __construct()
	{
		$this->createdAt = new DateTimeImmutable();
		$this->updatedAt = new DateTimeImmutable();
		$this->setUuid();
	}

    public function __toString(): string
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }


    public function getId(): ?int
	{
		return $this->id;
	}

	public function getUuid(): ?string
	{
		return $this->uuid;
	}

	public function setUuid(?string $uuid = null): static
	{
		if (is_null($uuid)) {
			$uuidGenerator = Uuid::v4();
			$this->uuid = $uuidGenerator->toRfc4122();
		} else {
			$this->uuid = $uuid;
		}

		return $this;
	}

	public function getUsername(): ?string
	{
		return $this->username;
	}

	public function setUsername(?string $username): static
	{
		$this->username = $username;

		return $this;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(?string $email): static
	{
		$this->email = $email;

		return $this;
	}

	public function getFirstname(): ?string
	{
		return $this->firstname;
	}

	public function setFirstname(?string $firstname): static
	{
		$this->firstname = $firstname;

		return $this;
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function setLastname(?string $lastname): static
	{
		$this->lastname = $lastname;

		return $this;
	}

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string
	{
		return (string) $this->uuid;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';

		return array_unique($roles);
	}

	public function setRoles(array $roles): static
	{
		$this->roles = $roles;

		return $this;
	}

	/**
	 * @see PasswordAuthenticatedUserInterface
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): static
	{
		$this->password = $password;

		return $this;
	}

	public function getPlainPassword(): ?string
	{
		return $this->plainPassword;
	}

	public function setPlainPassword(?string $plainPassword): static
	{
		$this->plainPassword = $plainPassword;

		return $this;
	}

	/**
	 * Returning a salt is only needed if you are not using a modern
	 * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
	 *
	 * @see UserInterface
	 */
	public function getSalt(): ?string
	{
		return null;
	}

	/**
	 * @see UserInterface
	 */
	public function eraseCredentials(): void
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

	public function isVerified(): bool
	{
		return $this->isVerified;
	}

	public function setIsVerified(bool $isVerified): static
	{
		$this->isVerified = $isVerified;

		return $this;
	}

    public function getGithubId(): ?string
    {
        return $this->githubId;
    }

    public function setGithubId(?string $githubId): static
    {
        $this->githubId = $githubId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): static
    {
        $this->googleId = $googleId;

        return $this;
    }

	public function getCreatedAt(): ?DateTimeImmutable
	{
		return $this->createdAt;
	}

	public function setCreatedAt(DateTimeImmutable $createdAt): static
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getUpdatedAt(): ?DateTimeImmutable
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt(DateTimeImmutable $updatedAt): static
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	public function getDeletedAt(): ?DateTimeImmutable
	{
		return $this->deletedAt;
	}

	public function setDeletedAt(?DateTimeImmutable $deletedAt): static
	{
		$this->deletedAt = $deletedAt;

		return $this;
	}

    /**
     * @param LifecycleEventArgs $args
     * @return void
     */
    #[ORM\PreUpdate]
    public function preUpdate(LifecycleEventArgs $args): void
    {
        /* @var User $object */
        $object = $args->getObject();
        if ($object instanceof User) {
            $object->setUpdatedAt(new \DateTimeImmutable('now'));
        }
    }
}
