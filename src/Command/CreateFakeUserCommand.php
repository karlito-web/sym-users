<?php

declare(strict_types=1);

namespace KarlitoWeb\Users\Command;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use KarlitoWeb\Users\Entity\User;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'users:create:fake',
    description: 'Create fake users',
    aliases: ['users-create-fake', 'u:c:f', 'u-c-f'],
    hidden: false,
)]
class CreateFakeUserCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
    )
    {
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Init Variables
        $io                 = new SymfonyStyle($input, $output);
        $em                 = $this->entityManager;
        $connection         = $em->getConnection();
        $choice             = $input->getArgument('choice');
        $count              = 0;

        // Start
        $io->section((string) self::getDefaultDescription());

        // Introduction
        if (is_null($choice)) {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Do you want to create users from Attack On Titan or Star Wars ?',
                ['aot', 'sw'],
                'aot',
            );

            $choice = $helper->ask($input, $output, $question);
        }

        // Get Users
        if ($choice === 'aot') {
            $users = self::getAOT();
        } elseif ($choice === 'sw') {
            $users = self::getSW();
        } else {
            $users = [];
        }

        // Create User
        try {
            $connection->beginTransaction();
            foreach ($users as $user) {
                $u = new User();
                $u
                    ->setUsername($user['username'])
                    ->setEmail($user['email'])
                    ->setFirstName($user['firstname'])
                    ->setLastName($user['lastname'])
                    ->setPlainPassword('password')
                    ->setPassword(
                        $this->userPasswordHasher->hashPassword($u, 'password')
                    )
                    ->setRoles(["ROLE_USER"])
                ;

                $r = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $user['email']]);
                if (is_null($r)) {
                    $this->entityManager->persist($u);
                    $count++;
                }
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
            $connection->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        // Conclusion
        $io->success('Add ' . $count . ' new users !');

        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        // Clear Display
        system('clear');


        // configure an argument
        $this->addArgument(
            'choice',
            InputArgument::OPTIONAL,
            'Attack On Titan or Star Wars'
        );
    }

    private static function getAOT(): array
    {
        return [
            ['username' => 'eren.yeager', 'email' => 'eren.yeager@aot.jp', 'firstname' => 'Eren', 'lastname' => 'Yeager'],
            ['username' => 'mikasa.ackerman', 'email' => 'mikasa.ackerman@aot.jp', 'firstname' => 'Mikasa', 'lastname' => 'Ackerman'],
            ['username' => 'armin.arlert', 'email' => 'armin.arlert@aot.jp', 'firstname' => 'Armin', 'lastname' => 'Arlert'],
            ['username' => 'livaï.ackerman', 'email' => 'livaï.ackerman@aot.jp', 'firstname' => 'Livaï', 'lastname' => 'Ackerman'],
            ['username' => 'erwin.smith', 'email' => 'erwin.smith@aot.jp', 'firstname' => 'Erwin', 'lastname' => 'Smith'],
            ['username' => 'hange.zoe', 'email' => 'hange.zoe@aot.jp', 'firstname' => 'Hange', 'lastname' => 'Zoe'],
            ['username' => 'reiner.braun', 'email' => 'reiner.braun@aot.jp', 'firstname' => 'Reiner', 'lastname' => 'Braun'],
            ['username' => 'annie.leonhart', 'email' => 'annie.leonhart@aot.jp', 'firstname' => 'Annie', 'lastname' => 'Leonhart'],
            ['username' => 'bertolt.hoover', 'email' => 'bertolt.hoover@aot.jp', 'firstname' => 'Bertolt', 'lastname' => 'Hoover'],
            ['username' => 'sasha.braus', 'email' => 'sasha.braus@aot.jp', 'firstname' => 'Sasha', 'lastname' => 'Braus'],
            ['username' => 'connie.springer', 'email' => 'connie.springer@aot.jp', 'firstname' => 'Connie', 'lastname' => 'Springer'],
            ['username' => 'historia.reiss', 'email' => 'historia.reiss@aot.jp', 'firstname' => 'Historia', 'lastname' => 'Reiss'],
            ['username' => 'sieg.jäger', 'email' => 'sieg.jäger@aot.jp', 'firstname' => 'Sieg', 'lastname' => 'Jäger'],
            ['username' => 'gabi.braun', 'email' => 'gabi.braun@aot.jp', 'firstname' => 'Gabi', 'lastname' => 'Braun'],
            ['username' => 'falco.grice', 'email' => 'falco.grice@aot.jp', 'firstname' => 'Falco', 'lastname' => 'Grice'],
            ['username' => 'pieck.finger', 'email' => 'pieck.finger@aot.jp', 'firstname' => 'Pieck', 'lastname' => 'Finger'],
            ['username' => 'porco.galliard', 'email' => 'porco.galliard@aot.jp', 'firstname' => 'Porco', 'lastname' => 'Galliard'],
            ['username' => 'dot.pyxis', 'email' => 'dot.pyxis@aot.jp', 'firstname' => 'Dot', 'lastname' => 'Pyxis'],
            ['username' => 'willy.tybur', 'email' => 'willy.tybur@aot.jp', 'firstname' => 'Willy', 'lastname' => 'Tybur'],
            ['username' => 'tom.xaver', 'email' => 'tom.xaver@aot.jp', 'firstname' => 'Tom', 'lastname' => 'Xaver'],
            ['username' => 'ymir.fritz', 'email' => 'ymir.fritz@aot.jp', 'firstname' => 'Ymir', 'lastname' => 'Fritz'],
        ];
    }

    private static function getSW(): array
    {
        return [
            ['username' => 'ahsoka.tano', 'email' => 'ahsoka.tano@star-wars.com', 'firstname' => 'Ahsoka', 'lastname' => 'Tano' ],
            ['username' => 'amiral.ackbar', 'email' => 'amiral.ackbar@star-wars.com', 'firstname' => 'Amiral', 'lastname' => 'Ackbar' ],
            ['username' => 'amiral.hux', 'email' => 'amiral.hux@star-wars.com', 'firstname' => 'Amiral', 'lastname' => 'Hux' ],
            ['username' => 'anakin.skywalker', 'email' => 'anakin.skywalker@star-wars.com', 'firstname' => 'Anakin', 'lastname' => 'Skywalker' ],
            ['username' => 'asajj.ventress', 'email' => 'asajj.ventress@star-wars.com', 'firstname' => 'Asajj', 'lastname' => 'Ventress' ],
            ['username' => 'boba.fett', 'email' => 'boba.fett@star-wars.com', 'firstname' => 'Boba', 'lastname' => 'Fett' ],
            ['username' => 'cad.bane', 'email' => 'cad.bane@star-wars.com', 'firstname' => 'Cad', 'lastname' => 'Bane' ],
            ['username' => 'comte.dooku', 'email' => 'comte.dooku@star-wars.com', 'firstname' => 'Comte', 'lastname' => 'Dooku' ],
            ['username' => 'dark.maul', 'email' => 'dark.maul@star-wars.com', 'firstname' => 'Dark', 'lastname' => 'Maul' ],
            ['username' => 'dark.vador', 'email' => 'dark.vador@star-wars.com', 'firstname' => 'Dark', 'lastname' => 'Vador' ],
            ['username' => 'han.solo', 'email' => 'han.solo@star-wars.com', 'firstname' => 'Han', 'lastname' => 'Solo' ],
            ['username' => 'kylo.ren', 'email' => 'kylo.ren@star-wars.com', 'firstname' => 'Kylo', 'lastname' => 'Ren' ],
            ['username' => 'lando.calrissian', 'email' => 'lando.calrissian@star-wars.com', 'firstname' => 'Lando', 'lastname' => 'Calrissian' ],
            ['username' => 'leia.organa', 'email' => 'leia.organa@star-wars.com', 'firstname' => 'Leia', 'lastname' => 'Organa' ],
            ['username' => 'luke.skywalker', 'email' => 'luke.skywalker@star-wars.com', 'firstname' => 'Luke', 'lastname' => 'Skywalker' ],
            ['username' => 'mace.windu', 'email' => 'mace.windu@star-wars.com', 'firstname' => 'Mace', 'lastname' => 'Windu' ],
            ['username' => 'obi-wan.kenobi', 'email' => 'obi-wan.kenobi@star-wars.com', 'firstname' => 'Obi-Wan', 'lastname' => 'Kenobi' ],
            ['username' => 'padme.amidala', 'email' => 'padme.amidala@star-wars.com', 'firstname' => 'Padmé', 'lastname' => 'Amidala' ],
            ['username' => 'qui-gon.jinn', 'email' => 'qui-gon.jinn@star-wars.com', 'firstname' => 'Qui-Gon', 'lastname' => 'Jinn' ]
        ];
    }
}
