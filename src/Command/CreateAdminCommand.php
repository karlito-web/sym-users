<?php

declare(strict_types=1);

namespace KarlitoWeb\Users\Command;

use Doctrine\ORM\EntityManagerInterface;
use KarlitoWeb\Users\Entity\User;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'users:create:admin',
    description: 'Create admin users',
    aliases: ['users-create-admin', 'u:c:a', 'u-c-a'],
    hidden: false,
)]
class CreateAdminCommand extends Command
{
    private SymfonyStyle $io;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $u = new User();
        $u
            ->setEmail(strval($input->getArgument('email')))
            ->setPlainPassword('password')
            ->setPassword(
                $this->userPasswordHasher->hashPassword($u, strval($input->getArgument('password')))
            )
            ->setRoles(["ROLE_ADMIN"])
        ;
        $this->entityManager->persist($u);
        $this->entityManager->flush();
        $this->entityManager->clear();

        return Command::SUCCESS;
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
         if (null !== $input->getArgument('email') && null !== $input->getArgument('password')) {
             return;
         }

         $this->io->text('Welcome to PhpStorm!');
         $this->askArgument($input, 'email');
         $this->askArgument($input, 'password', true);
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
       $this->io = new SymfonyStyle($input, $output);
    }

    protected function configure(): void
    {
        // Clear Display
        system('clear');

        // configure arguments
        $this->addArgument('email', InputArgument::class::OPTIONAL, 'Your email');
        $this->addArgument('password', InputArgument::class::OPTIONAL, 'Your password');
    }

    private function askArgument(InputInterface $input, string $name, bool $hidden = false): void
    {
        $value = strval($input->getArgument($name));

        if ('' !== $value) {
            $this->io->text(sprintf('> <info>%s</info>: $s', $name, $value));
        } else {
            $value = match ($hidden) {
                false => $this->io->ask($name),
                default => $this->io->askHidden($name),
            };
            $input->setArgument($name, $value);
        }
    }
}
