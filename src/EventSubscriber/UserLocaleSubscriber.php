<?php

namespace KarlitoWeb\Users\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;

class UserLocaleSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly RequestStack $requestStack,
    ) {
    }

    public function onLoginSuccess(LoginSuccessEvent $event): void
    {
        $user = $event->getUser();

        if (null !== $user->getFirstname()) {
            $this->requestStack->getSession()->set('firstname', $user->getFirstname());
        }

        if (null !== $user->getLastname()) {
            $this->requestStack->getSession()->set('lastname', $user->getLastname());
        }

        if (null !== $user->getUsername()) {
            $this->requestStack->getSession()->set('username', $user->getUsername());
        }

        if (null !== $user->getUuid()) {
            $this->requestStack->getSession()->set('uuid', $user->getUuid());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onLoginSuccess',
        ];
    }
}
