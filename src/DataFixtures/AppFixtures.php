<?php

namespace KarlitoWeb\Users\DataFixtures;

use KarlitoWeb\Users\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class AppFixtures
 *
 * @package KarlitoWeb\Users\DataFixtures
 */
class AppFixtures extends Fixture
{
	public function __construct(private readonly UserPasswordHasherInterface $userPasswordHasher) {}

	public function load(ObjectManager $manager): void
	{
		// SUPER ADMIN
		$gp = new User();
		$gp->setFirstname('Giancarlo');
		$gp->setLastname('PALUMBO');
		$gp->setUsername('Sako92');
		$gp->setEmail('giancarlo.palumbo@free.fr');
		$gp->setPlainPassword('giancarlo');
		$gp->setPassword(
			$this->userPasswordHasher->hashPassword($gp, 'giancarlo')
		);
		$gp->setRoles(['ROLE_SUPER_ADMIN']);
		$manager->persist($gp);

		// ADMIN
		$gp = new User();
		$gp->setFirstname('Anakin');
		$gp->setLastname('SKYWALKER');
		$gp->setUsername('master-jedi');
		$gp->setEmail('anakin.skywalker@clone-wars.fr');
		$gp->setPlainPassword('anakin');
		$gp->setPassword(
			$this->userPasswordHasher->hashPassword($gp, 'anakin')
		);
		$gp->setRoles(['ROLE_ADMIN']);
		$manager->persist($gp);

		// USER
		$gp = new User();
		$gp->setFirstname('Ahsoka');
		$gp->setLastname('TANO');
		$gp->setUsername('padawan');
		$gp->setEmail('ahsoka.tano@clone-wars.fr');
		$gp->setPlainPassword('ahsoka');
		$gp->setPassword(
			$this->userPasswordHasher->hashPassword($gp, 'ahsoka')
		);
		$gp->setRoles(['ROLE_USER']);
		$manager->persist($gp);

		// USERS
		$faker = Factory::create('it_IT');
		for ($i = 0; $i < 7; $i++) {
			$f = $faker->firstName;
			$l = $faker->lastName;
			$u = new User();
			$u->setUsername($faker->userName);
			$u->setEmail(strtolower($f.'.'.$l.'@'.$faker->freeEmailDomain));
			$u->setFirstname($f);
			$u->setLastname($l);
			$u->setPlainPassword('password');
			$u->setPassword(
				$this->userPasswordHasher->hashPassword($u, 'password')
			);
            $u->setRoles(['ROLE_USER']);
			$manager->persist($u);
		}

		$manager->flush();
	}
}
