<?php

declare(strict_types=1);

namespace KarlitoWeb\Users\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250207120142 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Google ID';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_user ADD COLUMN google_id VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__account_user AS SELECT id, uuid, username, email, firstname, lastname, roles, password, is_verified, github_id, created_at, updated_at, deleted_at FROM account_user');
        $this->addSql('DROP TABLE account_user');
        $this->addSql('CREATE TABLE account_user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, uuid VARCHAR(180) NOT NULL, username VARCHAR(64) DEFAULT NULL, email VARCHAR(255) NOT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(60) DEFAULT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) DEFAULT NULL, is_verified BOOLEAN DEFAULT 0 NOT NULL, github_id VARCHAR(255) DEFAULT NULL, created_at DATE NOT NULL --(DC2Type:date_immutable)
        , updated_at DATE NOT NULL --(DC2Type:date_immutable)
        , deleted_at DATE DEFAULT NULL --(DC2Type:date_immutable)
        )');
        $this->addSql('INSERT INTO account_user (id, uuid, username, email, firstname, lastname, roles, password, is_verified, github_id, created_at, updated_at, deleted_at) SELECT id, uuid, username, email, firstname, lastname, roles, password, is_verified, github_id, created_at, updated_at, deleted_at FROM __temp__account_user');
        $this->addSql('DROP TABLE __temp__account_user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_10051E3D17F50A6 ON account_user (uuid)');
    }
}
