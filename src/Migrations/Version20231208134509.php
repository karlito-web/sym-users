<?php

declare(strict_types=1);

namespace KarlitoWeb\Users\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231208134509 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Table for Accounts';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE account_reset_password_request (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER UNSIGNED NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , expires_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , CONSTRAINT FK_E41866D0A76ED395 FOREIGN KEY (user_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_E41866D0A76ED395 ON account_reset_password_request (user_id)');
        $this->addSql('CREATE TABLE account_user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, uuid VARCHAR(180) NOT NULL, username VARCHAR(64) DEFAULT NULL, email VARCHAR(255) NOT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(60) DEFAULT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) DEFAULT NULL, is_verified BOOLEAN DEFAULT 0 NOT NULL, created_at DATE NOT NULL --(DC2Type:date_immutable)
        , updated_at DATE NOT NULL --(DC2Type:date_immutable)
        , deleted_at DATE DEFAULT NULL --(DC2Type:date_immutable)
        )');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_10051E3D17F50A6 ON account_user (uuid)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE account_reset_password_request');
        $this->addSql('DROP TABLE account_user');
    }
}
