Security
============


### Step 1 - Install the bundle
Doc (https://symfony.com/doc/5.4/security.html)
``` cmd
composer require security bundle
```

### Step 2 - Make an User
Doc (https://symfony.com/doc/5.4/security.html#the-user)
1. Create an Entity  
2. Make a migration
``` cmd
php bin/console make:user
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```

### Step 3 - Create a Form
``` cmd
composer require symfonycasts/verify-email-bundle
php bin/console make:registration-form
php bin/console security:hash-password
```

### Step 4 - Authenticating Users
Doc (https://symfony.com/doc/5.4/security.html#authenticating-users)  
1. Create a Controller  
``` cmd
php bin/console make:controller Login
```

2. Edit security.yaml 
``` yaml
#        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
```
``` yaml
        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface:
            algorithm:              bcrypt
            cost:                   15
            encode_as_base64:       true
            memory_cost:            10   # Lowest possible value for argon
            time_cost:              3    # Lowest possible value for argon
```

Add under provider
``` yaml
    providers:
        kw_provider:
            entity:
                class: KarlitoWeb\Users\Entity\User
                property: email
```

Change
``` yaml
    firewalls:
        main:
            provider: users_in_memory
```
By
``` yaml
    firewalls:
        main:
            provider: kw_provider
```

Add
``` yaml
    firewalls:
        main:
            custom_authenticator: KarlitoWeb\Users\Security\DefaultAuthenticator

```

``` yaml
            logout:
#                path: app_logout
                path: kw.users.logout
                # where to redirect after logout
                # target: app_any_route
                target: kw.users.login
                clear_site_data: true
                delete_cookies: true
```


3. Create a Controller for JSON connection 
Doc (https://symfony.com/doc/5.4/security.html#json-login)
``` cmd
php bin/console make:controller --no-template ApiLogin
```

4. Create a route for log out
Doc (https://symfony.com/doc/5.4/security.html#logging-out)
``` yaml
    firewalls:
        main:
            logout:
                path: app_logout
```

5. Create a Customizing Logout   
Doc (https://symfony.com/doc/5.4/security.html#customizing-logout)
(EventListener -> LogoutSubscriber)   
```php
// src/EventListener/LogoutSubscriber.php
namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [LogoutEvent::class => 'onLogout'];
    }

    public function onLogout(LogoutEvent $event): void
    {
        // get the security token of the session that is about to be logged out
        $token = $event->getToken();

        // get the current request
        $request = $event->getRequest();

        // get the current response, if it is already set by another listener
        $response = $event->getResponse();

        // configure a custom logout response to the homepage
        $response = new RedirectResponse(
            $this->urlGenerator->generate('homepage'),
            RedirectResponse::HTTP_SEE_OTHER
        );
        $event->setResponse($response);
    }
}
```

### Step 6 -  
https://symfony.com/doc/5.4/security.html#fetching-the-user-object
Create Profile Controller

### Step 7 - 
https://symfony.com/doc/5.4/security.html#fetching-the-user-from-a-service
Create a Service

### Step 8 - Roles
Doc (https://symfony.com/doc/5.4/security.html#roles)

### Remember ME
Doc (https://symfony.com/doc/5.4/security/remember_me.html)
```php
$this->addSql('CREATE TABLE `rememberme_token` (
    `series` char(88) UNIQUE PRIMARY KEY NOT NULL,
    `value` varchar(88)  NOT NULL,
    `lastUsed` datetime NOT NULL,
    `class` varchar(100) NOT NULL,
    `username` varchar(200) NOT NULL
) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
$this->addSql('DROP TABLE rememberme_token');
```

### User Providers
https://symfony.com/doc/5.4/security/user_providers.html#security-entity-user-provider

### Password Hashing and Verification
https://symfony.com/doc/5.4/security/passwords.html

### DoctrineExtensions (SoftDelete, Timestamp, ...)
- Doc (https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html#writing-fixtures)  
- Github (https://github.com/doctrine-extensions/DoctrineExtensions/tree/main/doc)

### How to Write a Custom Authenticator
Doc (https://symfony.com/doc/5.4/security/custom_authenticator.html)
