Installation
============

## Step 1 - Download the Bundle
```console
git clone https://Karlito15@bitbucket.org/karlito-web/sym-users.git
```

## Step 2 - Enable the Bundle
Open the file : 
``` php
config\bundles.php
```

Add this line : 
``` php
    /* KarlitoWeb */
    KarlitoWeb\Users\UsersBundle::class => ['all' => true],
```
