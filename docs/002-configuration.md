Configuration
=============

### Step 1 - Edit  
``` yaml
```

### Step 2 - Edit  
``` yaml
```

### Step 3 - Edit /config/packages/doctrine.yaml  
Replace :
``` yaml
    dbal:
        driver:         pdo_mysql
        url: '%env(resolve:DATABASE_URL)%'
```
By
``` yaml
    dbal:
        driver:         pdo_mysql
        dbname:         '%env(resolve:DATABASE_NAME)%'
        host:           '%env(resolve:DATABASE_HOST)%'
        port:           '%env(resolve:DATABASE_PORT)%'
        user:           '%env(resolve:DATABASE_USER)%'
        password:       '%env(resolve:DATABASE_PASS)%'
        # IMPORTANT: You MUST configure your server version, either here or in the DATABASE_URL env var (see .env file)
        server_version:  '%env(resolve:DATABASE_SERVER_VERSION)%'
        charset: utf8
#        url: '%env(resolve:DATABASE_URL)%'
```
Add : 
``` yaml
    orm:
        mappings:
            KarlitoWeb\Users:
                is_bundle: false
                dir: '%kernel.project_dir%/bundle/Users/Entity'
                prefix: 'KarlitoWeb\Users\Entity'
                alias: Users
```

### Step 3 - Edit /config/packages/doctrine_migrations.yaml  
``` yaml
doctrine_migrations:
    migrations_paths:
        # namespace is arbitrary but should be different from App\Migrations
        # as migrations classes should NOT be autoloaded
        #'App\Migration': '%kernel.project_dir%/src/Migration'
        'KarlitoWeb\Users\Migration': '%kernel.project_dir%/bundle/Users/Migration'

    enable_profiler: true

    # Entity manager to use for migrations. This overrides the "connection" setting.
    em: default
    
    # Run all migrations in a transaction.
    all_or_nothing: true

    # Adds an extra check in the generated migrations to ensure that is executed on the same database type.
    check_database_platform: true

    # Path to your custom migrations template
    custom_template: ~

    # Organize migrations mode. Possible values are: "BY_YEAR", "BY_YEAR_AND_MONTH", false
    organize_migrations: false

    # Whether or not to wrap migrations in a single transaction.
    transactional: true

    storage:
        # Default (SQL table) metadata storage configuration
        table_storage:
            table_name: '__migrations__'
            version_column_name: 'version'
            version_column_length: 192
            executed_at_column_name: 'executed_at'
```

### Step 3 - Edit /config/packages/security.yaml  
``` yaml
security:
    role_hierarchy:
        ROLE_USER:              ~
        ROLE_ADMIN:             [ROLE_USER]
        ROLE_SUPER_ADMIN:       [ROLE_ADMIN]
    # https://symfony.com/doc/current/security.html#registering-the-user-hashing-passwords
    password_hashers:
        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 
#            algorithm: 'auto'
            algorithm: bcrypt
            cost: 15
            time_cost: 3 # Lowest possible value for argon
            memory_cost: 10 # Lowest possible value for argon
    # https://symfony.com/doc/current/security.html#loading-the-user-the-user-provider
    # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    providers:
        # used to reload user from session & other features (e.g. switch_user)
        app_user_provider:
            entity:
                class: KarlitoWeb\Users\Entity\LoginUser
                property: email
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            lazy: true
            provider: app_user_provider

            # activate different ways to authenticate
            # https://symfony.com/doc/current/security.html#the-firewall
            form_login:
                # "app_login" is the name of the route created previously
                login_path: app_login
                check_path: app_login
                enable_csrf: true
            logout:
                path: app_logout

            # https://symfony.com/doc/current/security/impersonating_user.html
            # switch_user: true

    # Easy way to control access for large sections of your site
    # Note: Only the *first* access control that matches will be used
    access_control:
        # - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }

when@test:
    security:
        password_hashers:
            # By default, password hashers are resource intensive and take time. This is
            # important to generate secure password hashes. In tests however, secure hashes
            # are not important, waste resources and increase test times. The following
            # reduces the work factor to the lowest possible values.
            Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface:
                algorithm: auto
                cost: 4 # Lowest possible value for bcrypt
                time_cost: 3 # Lowest possible value for argon
                memory_cost: 10 # Lowest possible value for argon
```

### Step 3 - Edit /config/packages/twig.yaml  
Add this line
``` yaml
    paths:
        '%kernel.project_dir%/src/Resources/template':                      'App'
        '%kernel.project_dir%/bundle/Layouts/Resources/template/aero':      'Aero'
        '%kernel.project_dir%/bundle/Layouts/Resources/template/bootstrap': 'Bootstrap'
        '%kernel.project_dir%/bundle/Users/Resources/template':             'Users'
```
