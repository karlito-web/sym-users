Command
=======

## Step 1 - Create an user
``` cmd
php bin/console make:auth
```
- Choose                  : Login form authenticator  
- Create Authenticator    : DefaultAuthenticator (/Security/DefaultAuthenticator) 
- Create Controller       : SecurityController   (/Controller/SecurityController)

## Step 2 - Edit Files

#### DefaultAuthenticator
Change
``` php
    public const LOGIN_ROUTE = 'app_login';
```
By
``` php
	public const LOGIN_ROUTE = 'kw.users.login';
```

Change
``` php
        // For example:
        // return new RedirectResponse($this->urlGenerator->generate('some_route'));
        throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
```
By
``` php
        // For example:
        // return new RedirectResponse($this->urlGenerator->generate('some_route'));
        // throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
        return new RedirectResponse($this->urlGenerator->generate('kw.users.dashboard.index'));
```

#### SecurityController
Change
``` php
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }
```
By
``` php
        /** Already Log */
		if ($this->getUser()) {
		    $this->addFlash('info', 'You are already logged !');
            return $this->redirectToRoute('kw.users.dashboard.index');
		}
```

## Step 3 - Create a form registration
``` cmd
php bin/console make:registration-form
```
- Choose : KarlitoWeb\Users\Entity\User
- Choose : email (3)
- Choose : yes
- Choose : webmaster@karlito-web.com
- Choose : Dark Vador
- Choose : no
- Choose : kw.users.login (8)

## Step 4 - Edit Files

#### RegistrationController
Change
``` php
```
By
``` php
```

Change
``` php
```
By
``` php
```

Change
``` php
```
By
``` php
```

Change
``` php
```
By
``` php
```

Change
``` php
```
By
``` php
```

## Step 9 - initialize the database and charge fixtures 
``` cmd
php bin/console doctrine:fixtures:load --append
```
